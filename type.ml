
(*lambda termes*)
type lb_terme =
	| Abs of string * lb_terme
	| App of lb_terme * lb_terme
	| Var of string

let print_lb_terme lbterme = 
	let rec aux abs = function
		| Abs (s, x) when abs -> "lb_"^s^". "^(aux true x)
		| Abs (s, x) -> "(lb_"^s^". "^(aux true x)^")"
		| App (Var x, Var y) -> x^" "^y
		| App (Var x, y) -> x^" ("^(aux true y)^")"
		| App (x, Var y) -> (aux false x)^" "^y
		| App (x, y) -> (aux false x)^" ("^(aux true y)^")"
		| Var s -> s
	in print_string (aux true lbterme^"\n")



(*termes de Bruijn*)
type bj_terme =
	| Lb of int*bj_terme
	| Ap of bj_terme * bj_terme
	| V of int*int*int

let rec print_bj_terme bjterme = 
	let rec aux abs = function
		| Lb (i, x) when abs -> 
                        "lb"^(string_of_int (i+1))^" "^(aux true x)
		| Lb (i, x) ->
                        "(lb"^(string_of_int (i+1))^" "^(aux true x)^")"
		| Ap (V (i0, j0, p0), V (i1, j1, p1)) ->
                        (aux false (V (i0, j0, p0)))^" "^(aux false (V (i1, j1, p1)))
		| Ap (V (i0, j0, p0), y) -> 
                        (aux false (V (i0, j0, p0)))^" ("^(aux true y)^")"
		| Ap (x, V (i1, j1, p1)) -> 
                        (aux false x)^" "^(aux false (V (i1, j1, p1)))
		| Ap (x, y) ->
                        "("^(aux true x)^") ("^(aux true y)^")"
                | V (0, 0, _) -> "< * >"
                | V (i, j, p) ->
                        "<"^(string_of_int i)^","^(string_of_int j)^">"^
                        (if p > 0 then "#"^(string_of_int p)^" " else "")
	in print_string (aux true bjterme^"\n")



(*contexte pour la construction des termes de Bruijn*)
type context =
	{mutable indice: int;
	indice_global: int;
	local: (string, int*int) Hashtbl.t;
	global: (int* (string, int*int) Hashtbl.t) list}

let context_find c x =
	try (c.indice,Hashtbl.find c.local x)
	with Not_found -> 
		let rec parcours = function 
			| (i, a)::q -> (try (i, Hashtbl.find a x)
						with Not_found -> parcours q)
			| [] -> failwith "Erreur inattendue"
		in parcours c.global


(* environements pour la kam*)
type environment = 
        | Ev
        | Env of (environment) * ((bj_terme * environment) array)
