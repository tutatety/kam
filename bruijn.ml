(* Indice de Bruijn *)
open Type


let rec bruijn lbterme = 
	let rec aux abs c = function
	| Abs (s0, Abs (s1, x)) when abs>0 ->
                c.indice <- c.indice + 1;
		Hashtbl.add c.local s0 (c.indice_global, c.indice);
                aux (abs+1) c (Abs (s1, x))
        | Abs (s0, x) when abs>0 ->
		c.indice <- c.indice+1;
		Hashtbl.add c.local s0 (c.indice_global, c.indice);
		Lb (abs, aux (abs+1) c x)
	| Abs (s0, Abs (s1, x)) ->
	                let c1 = {indice=1;
				indice_global = c.indice_global + 1;
				local = Hashtbl.create 10;
				global = (c.indice, c.local)::c.global}
			in Hashtbl.add c1.local s0 (c1.indice_global, 1);
			aux (abs+1) c1 (Abs (s1, x))
	| Abs (s0, x) ->
			let c1 = {indice=1;
			        indice_global = c.indice_global + 1;
	        		local = Hashtbl.create 10;
				global = (c.indice, c.local)::c.global}
			in Hashtbl.add c1.local s0 (c1.indice_global, 1);
			Lb (abs, aux (abs+1) c1 x)
	| App(x, y) ->
		Ap(aux 0 c x, aux 0 c y)
        | Var "*" -> V(0, 0, 0)
	| Var x ->
                let n = String.length x in
                let nv_x,p = (try 
                        let k = String.rindex_from x (n-1) '#' in
                        let nb_print = String.sub x (k+1) (n-k-1) in
                        let nv_x = String.sub x 0 k in
                nv_x, int_of_string nb_print
                with
                        | Not_found -> x, 0
                        | _ -> failwith "rentrer un nombre apres le #")
                in
		let a,(i,j) = context_find c nv_x
		in V (c.indice_global-i+1, a-j+1, p)
	in 
	let c ={indice = 1;
			indice_global = 1;
			local=Hashtbl.create 10;
                        global= []}
	in aux 0 c lbterme
