(* impl�mentation KAM *)
(* parseur *)

(* format des entr�es *)

(* abstractions : lb_x.u ou lb_x u *)
(* applications : u v *)
(* variables : x *)
(* parenth�ses *)
open Type

let premiere_coupe s = 
(* v�rifie les histoire de parenthesage et d�coupe selon �a*)
	let m = String.length s in
	let rec decoupe i p indice =
		if indice < 0 || (i+p > m && indice <> 0) then 
			failwith "Erreur de parenthesage";
		if i+p = m then [String.sub s i p]
		else begin
			match s.[i+p] with
				|'(' when indice = 0 ->
					(String.sub s i p)::(decoupe (p+i+1) 0 (indice + 1))
				|')' when indice = 1 ->
					(String.sub s i p)::(decoupe (p+i+1) 0 (indice - 1))
				|'(' -> decoupe i (p+1) (indice + 1)
				|')' -> decoupe i (p+1) (indice - 1)
				|' ' | '.' when indice = 0 ->
					(String.sub s i p)::(decoupe (p+i+1) 0 indice)
				|'l' when indice = 0 && i+p+3<m && s.[i+p+1]='b' && s.[i+p+2]='_' ->
					(String.sub s i 3)::(decoupe(i+3) (0) 0)
				|'_' when indice = 0 -> 
					failwith "Utilisation de '_' interdite dans les noms de variables"
				|' ' when p=0 -> decoupe (i+1) p indice
				|_ -> decoupe i (p+1) indice
		end;
	in
	List.filter (fun x-> x<>"") (decoupe 0 0 0)


let check_vars l =
	let rec aux vars = function
	| Abs (s, x) ->
                Abs (s, aux (s::vars) x)
	| App (x, y) -> App (aux vars x, aux vars y)
	| Var s ->
                let n = String.length s in
                let nv_s =(try
                        let k = String.rindex_from s (n-1) '#' in
                        String.sub s 0 k
                        with |Not_found -> s) in
               if List.exists (fun x-> x=nv_s) vars || nv_s = "*" then Var s
               else failwith ("Erreur variable "^nv_s^" non define")
	in aux [] l

let parseur s =
let rec parseur_aux s = 
	let condition_arret = 
		(String.contains s '.') ||
		(String.contains s ' ') ||
		(String.contains s '(') ||
		(String.contains s ')') ||
		(String.contains s '_') in
	if not condition_arret then Var s
	else
	begin
	let l = premiere_coupe s in
	let applications l = 
		let rec aux = function
			| [] -> failwith "Erreur syntaxe"
			| x::[] -> x
			| x::q -> App (aux q, x)
		in aux (List.rev l)
	in
	let rec traitement = function
		| x::[] when x = "lb_" -> failwith "Erreur syntaxe"
		| x::y::[] when x = "lb_" -> failwith "Erreur syntaxe"
		| x::y::z::q when x = "lb_"-> 
			(Abs (y, applications (traitement(z::q)))) :: []
		| x::q ->
			(parseur_aux x)::(traitement q)
		| [] -> []
	in
	applications (traitement l)
	end
in check_vars (parseur_aux s)
