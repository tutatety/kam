
let () = print_string "Exemple de terme: (lb_f lb_x f f f x) (lb_x x#1) * \n"
let data = input_line stdin
let () = print_string "\n"

let terme = Parseur.parseur data
let () = Type.print_lb_terme terme

let terme_bj = Bruijn.bruijn terme
let () = Type.print_bj_terme terme_bj

let nv_terme_bj = Kam.kam terme_bj
let () = print_string "\n"
let () = Type.print_bj_terme nv_terme_bj
