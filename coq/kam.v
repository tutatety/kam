
Inductive term :=
| Lb : term -> term
| Ap : term -> term -> term
| V : nat -> term
| Star : term.
 
Inductive term_erreur :=
  |T : term -> term_erreur
  |Erreur_bj : term_erreur.

Notation "*" := Star.

Inductive clist :=
  | Lv : clist
  | Lp : cloture -> clist -> clist
with cloture :=
  | C : term -> clist -> cloture
  | Erreur_cl : nat -> cloture.

Definition pile :=
  clist.

Definition environment :=
  clist.

Fixpoint cherche_var env i :=
  match env, i with
    | Lv, _ => Erreur_cl 10
    | Lp cl _, 0 => cl
    | Lp _ nenv, S i => cherche_var nenv i
  end.

Inductive resultat :=
  | Succes : term -> environment -> pile -> nat -> resultat
  | Echec : term -> environment -> pile -> nat -> resultat
  | Plus_de_fuel : resultat.

Fixpoint egal_int (i:nat) (j:nat) : bool :=
match i, j with
  | 0, 0 => true
  | S i, S j => egal_int i j
  | _, _ => false
end.
(*
Fixpoint egal_term (x y : term) : bool :=
match x, y with
  | Lb x, Lb y => egal_term x y
  | Ap x1 x2, Ap y1  y2 => andb (egal_term x1 y1) (egal_term x2 y2)
  | V i, V j => egal_int i j
  | _, _ => false
end.
*)
Fixpoint term_substitue t u i := match t with
  | Lb v => Lb (term_substitue v u (S i))
  | Ap v w => Ap (term_substitue v u i) (term_substitue w u i)
  | V j => if egal_int i j then u else V j
  | * => *
end.

(* Fonction qui calcule le réduit en 1 pas via wh-term_erreur_reduction_1_pas *)
Fixpoint term_reduction_1_pas (term:term) :term_erreur := 
match term with
  | Ap (Lb x) y => T (term_substitue x y 0)
  | Ap * y => Erreur_bj
  | Ap (V i) y => Erreur_bj
  | Ap x y => 
    match term_reduction_1_pas x with
      | T nx => T (Ap nx y)
      | Erreur_bj => Erreur_bj
    end
  | _ => Erreur_bj
end.

Definition term_erreur_reduction_1_pas : term_erreur -> term_erreur :=
  fun x => match x with 
    |Erreur_bj => Erreur_bj
    |T term => term_reduction_1_pas term
   end.
(* Relation entre term et réduit 
Definition red_prop : term_erreur -> term_erreur -> Prop :=
fun x y => term_erreur_reduction_1_pas x = y.

Lemma red_prop_reduces : 
forall t u, term_erreur_reduction_1_pas t = u <-> red_prop t u.
Proof.
intros T U.
split.
intro H.
apply H.
intro H.
apply H.
Qed.*)

Fixpoint term_erreur_whn_n_pas (x:term_erreur) (fuel:nat) : term_erreur :=
match fuel with 
| 0 => x
| S i => term_erreur_whn_n_pas (term_erreur_reduction_1_pas x) i
end.

Fixpoint term_whn_fuel (x:term) (fuel:nat) : term_erreur :=
match fuel with 
| 0 => (*if is_whn x then T x else*) Erreur_bj
| S i =>
match x with
  | Lb x => T (Lb x)
  | Ap * y => Erreur_bj
  | Ap (Lb x) y => term_whn_fuel (term_substitue x y 0) i
  | Ap (V x) y =>
    match term_whn_fuel y i with
      | T ny => T (Ap (V x) ny)
      | Erreur_bj => Erreur_bj
    end
  | Ap x y =>
    match term_whn_fuel x i with
      | T nx => term_whn_fuel (Ap nx y) i
      | Erreur_bj => Erreur_bj
    end
  | x => T x
end
end.

Definition whn : term_erreur -> nat -> term_erreur :=
  fun x i => match x with
    | T x => term_whn_fuel x i
    | Erreur_bj => Erreur_bj
  end.


Lemma aux_term_whn_fuel_with_fuel_lb :
  forall i t1 t2, term_whn_fuel (Ap (Lb t1) t2) (S i) = term_whn_fuel (term_substitue t1 t2 0) i.
Proof.
  intros.
  simpl.
  reflexivity.
Qed.

Lemma aux_term_whn_fuel_with_fuel_ap :
  forall i t1 t2 t3, term_whn_fuel (Ap (Ap t1 t2) t3) (S i) =
  match term_whn_fuel (Ap t1 t2) i with
  | T nx => term_whn_fuel (Ap nx t3) i
  | Erreur_bj => Erreur_bj
  end.
Proof.
  simpl.
  reflexivity.
Qed.

Lemma aux_term_whn_fuel_with_fuel_v :
  forall i n t1, term_whn_fuel (Ap (V n) t1) (S i) =
  match term_whn_fuel t1 i with
  | T ny => T (Ap (V n) ny)
  | Erreur_bj => Erreur_bj
  end.
Proof.
  simpl.
  reflexivity.
Qed.


Theorem term_whn_fuel_with_fuel :
  forall i t, term_whn_fuel t i <> Erreur_bj -> term_whn_fuel t i = term_whn_fuel t (S i).
Proof.
  intro i.
  induction i ; intros.
  - simpl in H.
    contradiction.
  - destruct t.
    * simpl.
      reflexivity.
    * destruct t1.
      -- destruct i.
         ** simpl in H.
            contradiction.
         ** rewrite aux_term_whn_fuel_with_fuel_lb.
            rewrite aux_term_whn_fuel_with_fuel_lb.
            simpl in H.
            apply IHi.
            apply H.
      -- rewrite aux_term_whn_fuel_with_fuel_ap.
         rewrite aux_term_whn_fuel_with_fuel_ap.
         simpl in H.
         cut (term_whn_fuel (Ap t1_1 t1_2) i <> Erreur_bj).
         ** intros.
            apply IHi in H0.
            rewrite <- H0.
            destruct (term_whn_fuel (Ap t1_1 t1_2) i).
            --- now apply IHi.
            --- reflexivity.
         ** case_eq (term_whn_fuel (Ap t1_1 t1_2) i).
            --- intros.
                discriminate.
            --- intros.
                now rewrite H0 in H.
      -- rewrite aux_term_whn_fuel_with_fuel_v.
         rewrite aux_term_whn_fuel_with_fuel_v.
         simpl in H.
         cut (term_whn_fuel t2 i <> Erreur_bj).
         ** intros.
            apply IHi in H0.
            rewrite <- H0.
            destruct (term_whn_fuel t2 i).
            --- reflexivity.
            --- reflexivity.
         ** case_eq (term_whn_fuel t2 i).
            --- intros.
                discriminate.
            --- intros.
                now rewrite H0 in H.
      -- now simpl.
    * now simpl.
    * now simpl.
Qed.



Lemma post_term_whn_fuel :
  forall i t, term_erreur_reduction_1_pas (term_whn_fuel t i) = Erreur_bj.
Proof.
  intro.
  induction i.
  - intro.
    now simpl.
  - intro.
    destruct t.
    * now simpl.
    * destruct t1.
     -- apply IHi.
     -- simpl.
        destruct (term_whn_fuel (Ap t1_1 t1_2) i).
       ** apply IHi.
       ** now simpl.
     -- simpl.
        cut (term_erreur_reduction_1_pas (term_whn_fuel t2 i) = Erreur_bj).
       ** intro.
          destruct (term_whn_fuel t2 i).
          --- simpl.
              now simpl in H.
          --- now simpl.
       ** apply IHi.
     -- simpl.
        cut (term_erreur_reduction_1_pas (term_whn_fuel t2 i) = Erreur_bj).
        ** intro.
           destruct (term_whn_fuel t2 i).
           --- now simpl.
           --- now simpl.
        ** apply IHi.
    * now simpl.
    * now simpl.
Qed.

Lemma post_whn :
  forall i t, term_erreur_reduction_1_pas (whn t i) = Erreur_bj.
Proof.
  intros.
  unfold whn.
  destruct t.
  - apply post_term_whn_fuel.
  - now simpl.
Qed.
(*
Lemma whn_normalizes :
  forall t v,(v <> Erreur_bj) /\ (exists fuel, whn t fuel = v) 
    -> not (exists u, u <> Erreur_bj /\ red_prop v u ).
Proof.
  intros.
  destruct H.
  destruct H0.
  intro.
  destruct H1.
  destruct H1.
  destruct H0.
  unfold red_prop in H2.
  rewrite post_whn in H2.
  now rewrite <- H2 in H1.
Qed.
*)
Fixpoint aux_closed (term:term) (i:nat) :Prop :=
  match term with
  | Ap x y => aux_closed x i /\ (aux_closed y i)
  | Lb x => aux_closed x (i+1)
  | V j => j < i
  | * => True
  end.

Definition closed : term_erreur -> Prop :=
  fun x => match x with
  | T x => aux_closed x 0
  | Erreur_bj => False
  end.

(* changer pour -> bool*)

(******************************************)
(** jusqu'ici, normalement plutôt simple  *)
(******************************************)

Require Import Program.
Require Import Coq.Init.Peano.
Require Import Coq.Arith.Lt.
Require Import Coq.Arith.Plus.

Inductive one_step_res :=
| Ok : environment -> pile -> term -> one_step_res
| Pas_ok : one_step_res.


Definition kam_onestep env pile term :=
  match term, pile with
  | Ap t u, _ =>  Ok env (Lp (C u env) pile) t
  | Lb x, Lp a q => Ok (Lp a env) q x
  | V i, _ => 
      match cherche_var env i with
      | C term n_env => Ok n_env pile term
      | _ => Pas_ok
      end
  | _, _ => Pas_ok
  end.

 Fixpoint  kam_nstep env pile term n :=
   match n with
   | 0 => Succes term env pile n
   | S m =>
       match kam_onestep env pile term with
       | Ok e s t => kam_nstep e s t m
       | Pas_ok => Echec term env pile (S m)
       end
   end.

Fixpoint kam env pile term fuel {struct fuel} : resultat :=
  match fuel with |0 => Plus_de_fuel
  | S n =>
  match term with
    | Ap t u =>
      kam env (Lp (C u env) pile) t n
    | Lb x =>
      match pile with
        | Lv =>
          Succes (Lb x) env pile n
        | Lp a q =>
          kam (Lp a env) q x n
      end
    | * =>
      match pile with
        |Lv => Succes * env Lv n (* tt va bien *)
        | _ => Echec * env pile n
      end
    | V i => 
      let x := cherche_var env i in
      match x with
        | C term n_env =>
          kam n_env pile term n
        | _ => Echec (V i) env pile n
        end
  end
  end.

Check kam.

Definition kam_s term :=
  kam Lv Lv term 1000.

Check kam_s.

Definition id := Lb (V 0).
Definition delta := Lb (Ap (V 0) (V 0)).

Eval compute in kam_s (Ap delta id).

Fixpoint taille_term term :=
match term with
  | Lb x => S (S (taille_term x))
  | Ap x y => S (taille_term x + (taille_term y))
  | V i => 1
  | * => 1
end.

Fixpoint taille_env env :=
  match env with
    | Lv => 1
    | Lp cl nenv => S (taille_env nenv) + taille_cl cl
  end
with taille_cl cl :=
  match cl with
    | C t nenv => taille_env nenv + taille_term t
    | Erreur_cl _ => 0
  end.

Lemma cherche_var_diminue_taille :
  forall i env, taille_cl (cherche_var env i) < taille_env env.
Proof.
  induction i.
  - destruct env.
    * simpl.
      apply lt_0_Sn.
    * simpl.
      rewrite plus_n_Sm.
      rewrite plus_comm.
      apply lt_plus_trans.
      apply lt_n_Sn.
  - destruct env.
    * simpl.
      apply lt_O_Sn.
    * simpl.
      rewrite plus_n_Sm.
      apply lt_plus_trans.
      apply IHi.
Qed.

Program Fixpoint aux_expansion
        (term:term) (env:environment)
        {measure (taille_env env + taille_term term)} :=
match term with
  | Lb x => Lb (aux_expansion x (Lp (Erreur_cl 100) env))
  | Ap x y =>
    Ap (aux_expansion x env)
       (aux_expansion y env)
  | V i =>
      match cherche_var env i with
        | C t nenv => aux_expansion t nenv
        | Erreur_cl _ => V i
      end
  | * => *
end.
Next Obligation.
  simpl.
  rewrite <- plus_n_Sm.
  rewrite <- plus_n_Sm.
  unfold "<".
  rewrite <- plus_n_O.
  reflexivity.
Qed.
Next Obligation.
  simpl.
  apply plus_lt_compat_l.
  rewrite plus_n_Sm.
  rewrite plus_n_O at 1.
  apply plus_lt_compat_l.
  apply lt_O_Sn.
Qed.
Next Obligation.
  simpl.
  rewrite plus_n_Sm.
  apply plus_lt_compat_l.
  rewrite plus_comm.
  apply lt_plus_trans.
apply lt_n_Sn.
Qed.
Next Obligation.
  simpl.
  apply lt_plus_trans.
  assert (taille_env nenv + (taille_term t) = taille_cl (C t nenv)).
  - now simpl.
  - rewrite H.
    rewrite Heq_anonymous.
    apply cherche_var_diminue_taille.
Qed.

Definition expansion term env:= T (aux_expansion term env).



Eval compute in delta.
Eval compute in kam_s (Ap (Lb (Lb (Ap (V 1) (V 0)))) id).
Eval compute in match kam_s (Ap (Lb (Lb (Ap (V 1) (V 0)))) id) with
  | Plus_de_fuel => ( *,* )
  | Echec t env pile _ => (aux_expansion t env, t)
  | Succes t env pile _ => (aux_expansion t env, t)
end.
Eval compute in term_substitue (Lb (Ap (V 1) (V 0))) id 0.
Eval compute in term_whn_fuel (Ap (Lb (Lb (Ap (V 1) (V 0)))) id) 1000.

Theorem kam_correct :
  forall t v, aux_closed t 0 -> (exists fuel, term_whn_fuel t fuel = T v)
  -> exists n t' E' S' m, kam Lv Lv t n = Succes t' E' S' m
        /\ expansion t' E' = T v.
Proof.
  intros.
  destruct H1.
  exists (x+x).
  
Admitted.


(*

→ ß-whn
> kam

t -1→ u  ⇒ "t"  >+  ?
             ↓       ↓   
             ?      "u"? 


u -n→ v  ⇒ "u"  >+  ?
             ↓       ↓   
             ?      "v"? 




t -ß→ u := ∃n, t -n→ u




t u * π * E > t * (u,E)::π * E 
    ↓              ↓
(([t,E]  [u,E]).... [π,E] .... ) 



*)
 
