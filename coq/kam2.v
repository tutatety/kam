(* Librairies coq *)
Require Import Program.
Require Import Coq.Init.Nat.
Require Import Coq.Init.Peano.
Require Import Coq.Arith.Lt.
Require Import Coq.Arith.Le.
Require Import Coq.Arith.Plus.

(* types *)
Inductive term :=
| Lb : term -> term
| Ap : term -> term -> term
| V : nat -> term
| Star : term.

Inductive term_error :=
  |T : term -> term_error
  |Failure_bj : term_error.

Notation "*" := Star.

Inductive clist :=
  | Lv : clist
  | Lp : cloture -> clist -> clist
with cloture :=
  | C : term -> clist -> cloture
  | Failure_cl : nat -> cloture.

Inductive pile :=
  | Pv : pile
  | Pp : term -> clist -> pile -> pile.

Definition environment :=
  clist.

Fixpoint env_find env i :=
  match env, i with
    | Lv, _ => Failure_cl 10
    | Lp cl _, 0 => cl
    | Lp _ nenv, S i => env_find nenv i
  end.

Fixpoint term_substitution (t u :term) (i:nat) : term := 
  match t with
    | Lb v => Lb (term_substitution v u (S i))
    | Ap v w => Ap (term_substitution v u i) (term_substitution w u i)
    | V j => if eqb i j then u else V j
    | * => *
  end.

(* Fonction qui calcule le réduit en 1 pas via wh-normalisation *)
Fixpoint term_reduction_1_step (term:term) : term_error := 
  match term with
    | Ap (Lb t) u => T (term_substitution t u 0)
    | Ap * _ => Failure_bj
    | Ap (V i) _ => Failure_bj
    | Ap t u => 
      match term_reduction_1_step t with
        | T t' => T (Ap t' u)
        | Failure_bj => Failure_bj
      end
    | _ => Failure_bj
  end.

Definition term_error_reduction_1_step : term_error -> term_error :=
  fun t => match t with 
    |Failure_bj => Failure_bj
    |T term => term_reduction_1_step term
   end.

Fixpoint term_error_whn_n_step (t:term_error) (n:nat) : term_error :=
  match n with 
    | 0 => t
    | S i => term_error_whn_n_step (term_error_reduction_1_step t) i
  end.

Lemma whn_n_m_step :
  forall n m t u v,
    (term_error_whn_n_step t n = u) /\
    (term_error_whn_n_step u m = v) ->
    term_error_whn_n_step t (n+m) = v.
  Admitted.

Lemma whn_Sn_step :
    forall n t v,
    term_error_whn_n_step t (S n) = v ->
      exists u,
      (term_error_whn_n_step t n = u) /\
      (term_error_reduction_1_step u = v).
  Admitted.

(* [É] :  Attention : (x) t ou ((x) t) u  sont en whn (mais pas (((λx.t)u)v)) ! 
   Ci-dessous une correction possible. *)

Fixpoint left_is_not_fun term : bool :=
  match term with
  | Lb _ => false
  | Ap t _ => left_is_not_fun t
  | _ => true
  end.

Definition term_is_whn (term:term) : bool :=
  match term with
    | Ap t u => left_is_not_fun t
    | _ => true
  end.

Eval compute in (term_is_whn (Ap (Ap (Lb (V 0)) (V 2) ) (V 3))).

(* Definition term_is_whn (term:term) : bool := *)
(*   match term with *)
(*     | Ap _ _ => false *)
(*     | _ => true *)
(*   end. *)

Fixpoint term_whn_fuel (t:term) (fuel:nat) : term_error :=
  if term_is_whn t then T t 
  else 
    match fuel with
      | 0 => Failure_bj
      | S i => 
        match term_reduction_1_step t with
          | T t' => term_whn_fuel t' i
          | _ => Failure_bj
        end
    end.

Definition term_error_whn_fuel : term_error -> nat -> term_error :=
  fun t i => match t with
    | T t' => term_whn_fuel t' i
    | _ => Failure_bj
  end.

(* terme clos *)
Fixpoint aux_closed (term:term) (i:nat) : bool :=
  match term with
  | Ap t u => andb (aux_closed t i) (aux_closed u i)
  | Lb x => aux_closed x (i+1)
  | V j => ltb j i
  | * => true
  end.

Definition closed : term_error -> bool :=
  fun x => match x with
  | T x => aux_closed x 0
  | Failure_bj => false
  end.

(* kam *)

Inductive kam_result :=
  | Success : term ->  pile -> environment -> kam_result
  | Failure : term ->  pile -> environment -> kam_result
  | Lack_fuel : kam_result.

Definition kam_1_step (term:term) (stack:pile) (env:environment) : kam_result :=
  match term, stack with
  | Ap t u, _ =>  Success t (Pp u env stack) env
  | Lb t, Pp t' env' tail => Success t tail (Lp (C t' env') env)
  | V i, _ => 
      match env_find env i with
      | C term n_env => Success term stack n_env
      | _ => Failure term stack env
      end
  | _, _ => Failure term stack env
  end.

Fixpoint  kam_n_step (term:term) (stack:pile) (env:environment) (n:nat) : kam_result:=
  match n with
    | 0 => Success term stack env
    | S n' =>
      match kam_1_step term stack env with
        | Success t s nenv => kam_n_step t s nenv n'
        | _ => Failure term stack env
      end
  end.

Lemma kam_n_m_step :
  forall n m t u v stack stack' stack'' env env' env'',
    (kam_n_step t stack env n = Success u stack' env') ->
    (kam_n_step u stack' env' m = Success v stack'' env'') ->
    kam_n_step t stack env (n+m) = Success v stack'' env''.
  Admitted.

Lemma kam_m_n_step :
    forall n m t v stack stack'' env env'',
    kam_n_step t stack env (n+m) = Success v stack'' env'' ->
      exists u stack' env',
      (kam_n_step t stack env n = Success u stack' env') /\
      (kam_n_step u stack' env' m = Success v stack'' env'').
  Admitted.

Fixpoint kam_fuel (term:term) (stack:pile) (env:environment) (fuel:nat) : kam_result :=
  match kam_1_step term stack env, fuel with
    | Success _ _ _, 0 => Lack_fuel
    | _, 0 => Success term stack env
    | Success t s nenv, S i => kam_fuel t s nenv i
    | _, _ => Failure term stack env
  end.

Fixpoint term_size (term:term) : nat :=
match term with
  | Lb t => S (term_size t)
  | Ap t u => S (term_size t + (term_size u))
  | V i => 1
  | * => 1
end.

Fixpoint env_size env :=
  match env with
    | Lv => 1
    | Lp cl nenv => S (env_size nenv) + cl_size cl
  end
with cl_size cl :=
  match cl with
    | C t nenv => env_size nenv
    | Failure_cl _ => 0
  end.

Lemma env_find_decrease_size :
  forall i env, cl_size (env_find env i) < env_size env.
Proof.
  induction i.
  - destruct env.
    * simpl.
      apply lt_n_Sn.
    * simpl.
      rewrite plus_n_Sm.
      rewrite plus_comm.
      apply lt_plus_trans.
      apply lt_n_Sn.
  - destruct env.
    * simpl.
      apply lt_n_Sn.
    * simpl.
      rewrite plus_n_Sm.
      apply lt_plus_trans.
      apply IHi.
Qed.

Program Fixpoint aux_expansion (env:environment) {measure (env_size env)}:=
  fix aux_aux (term:term) (prof:nat) :=
  match term with
    | Lb t => Lb (aux_aux t (S prof))
    | Ap t u => Ap (aux_aux t prof) (aux_aux u prof)
    | V i =>
      match env_find env (i-prof) with
        | C t nenv => aux_expansion nenv t prof
        | Failure_cl _ => V i
      end
    | * => *
  end.
Next Obligation.
  assert (env_size nenv = cl_size (C t nenv)).
  - now simpl.
  - rewrite H.
    rewrite Heq_anonymous.
    apply env_find_decrease_size.
Qed.

Check aux_expansion.

Fixpoint expansion (t:term) (s:pile) (env:environment) : term :=
  match s with
    | Pv => aux_expansion env t 0
    | Pp t' env' q => Ap (expansion t q env) (aux_expansion env' t' 0)
  end.

Lemma expansion_empty_env :
  forall t, aux_expansion Lv t 0 = t.
  Admitted.

Lemma one_step_reduction_vers_kam :
  forall u v u' env stack, term_error_reduction_1_step (T u) = T v
    -> expansion u' stack env = u
    -> exists v' stack' env' k,
      kam_n_step u' stack env k = Success v' stack' env'
      /\ expansion v' stack' env' = v.
  Admitted.


Theorem n_pas_reduction_vers_kam :
  forall n t v, term_error_whn_n_step (T t) n = T v
    -> exists v' stack env p, kam_n_step t Pv Lv p = Success v' stack env 
      /\ expansion v' stack env = v.
  induction n.
  Proof.
  - simpl.
    intros.
    exists t.
    exists Pv.
    exists Lv.
    exists 0.
    simpl.
    split.
    * reflexivity.
    * assert (t = v).
      -- now injection H.
      -- rewrite H0.
        apply expansion_empty_env.
  - intros t v.
    intro.
    apply whn_Sn_step in H.
    destruct H as [x].
    destruct H.
    destruct x.
    * apply IHn in H.
      destruct H as [t0'].
      destruct H as [stack'].
      destruct H as [env'].
      destruct H as [k1].
      destruct H.
      apply one_step_reduction_vers_kam 
        with (u' := t0') (env := env') (stack := stack') in  H0.
     -- destruct H0 as [v'].
        destruct H0 as [stack].
        destruct H0 as [env].
        destruct H0 as [k2].
        destruct H0.
        apply kam_n_m_step
          with (env := Lv) (stack := Pv) (t := t) (n := k1 )in H0.
       ** exists v'.
          exists stack.
          exists env.
          exists (k1 + k2).
          now split.
       ** apply H.
     -- apply H1.
    * now simpl in H0.
  Qed.
