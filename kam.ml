open Type

let rec cherche_var env u v = match env, u, v with
        | Ev, _, _ -> failwith "kam: terme non clos"
        | Env(_,nv), 1, v -> nv.(v-1)
        | Env (nenv,_), u, v -> cherche_var nenv (u-1) v 

let kam bj_terme = 

        let rec aux env stack = function
                | Ap (t, u) -> 
                        aux env ((u, env)::stack) t
                | Lb (i, x) -> 
                        let nv = Array.make (i+1) (V (0, 0, 0), Ev) in
                        let rec parcours = function
                                | j, q when j = i+1 -> q
                                | _, [] -> failwith "kam: manque de variables"
                                | j, a::q -> 
                                        nv.(i-j) <- a; 
                                        parcours (j+1, q)
                        in
                        let n_stack = parcours (0, stack) in
                        aux (Env (env,nv)) n_stack x  
                | V (0, 0, p) when stack = [] -> V (0, 0, p)
                | V (0, 0, p) -> failwith "kam: rien ne doit etre applique a *"
                | V (u, v, p) ->
                        let terme, n_env =cherche_var env u v in
                        if p > 0 then print_int p;
                        aux n_env stack terme
        in 
        
        aux Ev [] bj_terme
